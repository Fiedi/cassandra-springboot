package com.fiedi.acaisoft.validation;

import com.fiedi.acaisoft.exception.WrongMessageException;
import com.fiedi.acaisoft.model.Message;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

@Component
public class MessageValidator {


    public Message validate(Message message) {

        if(message.getEmail() == null || message.getEmail().isEmpty()) {
            throw new WrongMessageException("No email");
        } else
        if(!validateEmail(message.getEmail())) {
            throw new WrongMessageException("Mail is not valid");
        } else
        if(message.getMagicNumber() == null) {
            throw new WrongMessageException("No magic number");
        }

        return message;
    }

    private boolean validateEmail(String email) {
        EmailValidator emailValidator = EmailValidator.getInstance();
        return emailValidator.isValid(email);
    }

}
