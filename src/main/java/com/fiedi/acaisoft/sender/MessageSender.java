package com.fiedi.acaisoft.sender;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.fiedi.acaisoft.model.Message;

@Component
public class MessageSender {
	
	@Value("${mail.username}")
	private String username;
	@Value("${mail.password}")
	private String password;
	
	public List<Message> sendMessages (@RequestBody List<Message> messages) {
		messages.forEach(message -> {
				try {
					sendMessage(message);
				} catch (AddressException e) {
					e.printStackTrace();
				} catch (MessagingException e) {
					e.printStackTrace();
				}
		});
		return messages;
	}
	
	private void sendMessage (Message message) throws AddressException, MessagingException {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth","true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
	
	Session session = Session.getInstance(properties,
			new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username,password);
				}
			});
	
		javax.mail.Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(username, false));
		msg.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(message.getEmail()));
		msg.setSubject(message.getTitle());
		msg.setContent(message.getContent(),"text/html");
		
		Transport.send(msg);
	}

}
