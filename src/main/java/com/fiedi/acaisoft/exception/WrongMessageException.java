package com.fiedi.acaisoft.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class WrongMessageException extends RuntimeException {
	
	public WrongMessageException (String message) {
		super(message);
	}
	

}

