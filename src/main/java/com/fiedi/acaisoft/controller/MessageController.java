package com.fiedi.acaisoft.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fiedi.acaisoft.model.Message;
import com.fiedi.acaisoft.service.MessageService;


@RestController
@RequestMapping("/api")
public class MessageController {
	

	@Autowired
	MessageService messageService;
	
	@PostMapping("/message")
	public Message addMessage(@RequestBody Message message) {
		return messageService.add(message);
	}
	
	/*
	 * @GetMapping("/messages") public List<Message> getMessages(@RequestBody
	 * Message message) { return messageRepository.findAll(); }
	 */
	
	@PostMapping("/send")
	public String sendMessage(@RequestBody Message message) {
		return messageService.send(message.getMagicNumber());
	}
	
	@GetMapping("/messages/{email}")
	public List<Message> getMessagesByEmail(@PathVariable(value="email") String email) {
		return messageService.findByEmail(email);
	}
	
}
