package com.fiedi.acaisoft.model;

import lombok.NonNull;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
	
	@PrimaryKey
	private int id;
	private String email;
	private String title;
	private String content;
	private Integer magicNumber;
	
	public int getId() {
		return id;
	}
	public String getEmail() {
		return email;
	}
	public String getTitle() {
		return title;
	}
	public String getContent() {
		return content;
	}
	public Integer getMagicNumber() {
		return magicNumber;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setMagicNumber(Integer magicNumber) { this.magicNumber = magicNumber; }


	public Message withId(int id) {
		setId(id);
		return this;
	}

}
