package com.fiedi.acaisoft.repository;

public interface CustomizedMessageSave <Message> {
    <S extends Message> S save(S entity);
}
