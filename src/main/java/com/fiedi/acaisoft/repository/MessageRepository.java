package com.fiedi.acaisoft.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import com.fiedi.acaisoft.model.Message;

@Repository
public interface MessageRepository extends CassandraRepository<Message, Integer>, CustomizedMessageSave<Message> {

	@Query("SELECT * FROM mykeyspace.Message Where email=?0 ALLOW FILTERING")
	List<Message> findByEmail(String email);

	@Query("SELECT * FROM mykeyspace.Message Where magicNumber=?0 ALLOW FILTERING")
	List<Message> findByMagicNumber(Integer magicNumber);

}