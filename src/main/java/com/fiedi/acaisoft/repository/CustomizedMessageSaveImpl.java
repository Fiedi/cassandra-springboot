package com.fiedi.acaisoft.repository;

import com.fiedi.acaisoft.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.InsertOptions;

public class CustomizedMessageSaveImpl<Message> implements CustomizedMessageSave<Message>{

    @Autowired
    private CassandraOperations operations;

    @Override
    public <S extends Message> S save(S entity) {
        InsertOptions insertOptions = org.springframework.data.cassandra.core.InsertOptions.builder().ttl(300).build();
        operations.insert(entity, insertOptions);
        return entity;
    }
}
