package com.fiedi.acaisoft.service.impl;

import java.util.List;
import java.util.UUID;

import com.fiedi.acaisoft.exception.ResourceNotFoundException;
import com.fiedi.acaisoft.validation.MessageValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fiedi.acaisoft.sender.MessageSender;
import com.fiedi.acaisoft.model.Message;
import com.fiedi.acaisoft.repository.MessageRepository;
import com.fiedi.acaisoft.service.MessageService;

@Component
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	MessageRepository messageRepository;

	@Autowired
	MessageValidator messageValidator;
	
	@Autowired
	MessageSender messageSender;
	
	public Message add(Message message) {
		Message validatedMessage = messageValidator.validate(message);
		validatedMessage.withId(generateId());

		if(validatedMessage.getContent() == null) validatedMessage.setContent("");
		if(validatedMessage.getTitle() == null) validatedMessage.setTitle("");

		return messageRepository.save(validatedMessage);
	}

	@Override
	public List<Message> findByEmail(String email) {
		List<Message> messages = messageRepository.findByEmail(email);
				if(messages.isEmpty()) {
					throw new ResourceNotFoundException("No messages for email: "+email);
				}
		return messages;
	}

	@Override
	public String send(Integer magicNumber) {
		String report="";
		List<Message> messagesToSend = messageRepository.findByMagicNumber(magicNumber);
		if(messagesToSend.isEmpty()) throw new ResourceNotFoundException("No messages for magic number "+ magicNumber);

		List<Message> messagesSended = messageSender.sendMessages(messagesToSend);
		messagesSended.forEach(message -> {
			messageRepository.delete(message);
		});
		
		if(messagesSended.containsAll(messagesToSend)) 
			report = "All messages were sent succesfuly";
		 else {
			report = "Messages with id: ";
			messagesToSend.removeAll(messagesSended);

			report = report + " didn't send";
		 	  }
		
		return report;
	}

	private int generateId() {
		UUID idOne = UUID.randomUUID();
		String str=""+idOne;
		int uid=str.hashCode();
		String filterStr=""+uid;
		str=filterStr.replaceAll("-", "");
		return Integer.parseInt(str);
	}

}
