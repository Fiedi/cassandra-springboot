package com.fiedi.acaisoft.service;

import java.util.List;

import com.fiedi.acaisoft.model.Message;

public interface MessageService {
	
	public Message add(Message message);
	
	public List<Message> findByEmail(String email);
	
	public String send(Integer magicNumber);

}
